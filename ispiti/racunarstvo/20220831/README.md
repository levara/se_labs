# Pismeni ispit 31.08.2022.


## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.



## Zadaci

### 1. Pokrenuti projekt (2boda)

- kreirati virtualno okruženje
- aktivirati ga
- instalirati potrebne pakete pomoću `requirements.txt` datoteke koja se nalazi
  u `se_labs/ispiti/racunarstvo/20220209/requirements.txt`
  - podsjetimo se, instalacija paketa radi se iz odgovarajućeg direktorija
    pomoću `pip install -r requirements.txt` naredbe
- pokrenuti aplikaciju
- dodati sliku kao *admin*
  - username: `admin`
  - password: `admin`
- dodati sliku kao *student* (običan user)
  - username: `student`
  - password: `asdfasdf.1A`
- dodati bazu (db.sqlite3) u commit

### 2. Premjestiti image metadata (2 boda) 

- Izmjeniti izgled podataka o pojedinom imageu na index stranici da bude kao
  na slici:

![slika](https://imgur.com/vtEraDy.png)


### 3. Prebaciti 'Submit new image' u navbar (2 boda)

- Na `index` stranici treba maknuti 'Submit new image' link i treba ga
  prebaciti u navigaciju s lijeve strane, pored 'Home'


### 4. Dodaj cijenu na pojedinu sliku (4 boda)

- Omogući dodavanje cijene za svaku sliku
- Cijenu je dozvoljeno dodati u adminu, nije potrebno sučelje za cijenu
- Prikaži cijenu slike u zagradi nakon naslova slike na svim stranicama gdje se prikazuju slike

### 5. Dodaj sučelje za moderiranje komentara (6 bodova)

- Ulogiranom adminu se u navbaru, pored `Admin Dashboard` tipke pokazuje tipka
  `Comments moderation`
- Klik na tu tipku otvara novo sučelje u kojem su prikazane sve slike, te su
  pored svake slike vidljivi svi komentari na tu sliku.
- Za svaki komentar piše tekst `odobreno` ako je već odobren, ili tipka `Approve` ako nije odobren

### 6. Odobravanje komentara iz sučelja redirecta ponovno na sučelje  (4 bodova)

- Postojeća `approve` metoda u viewovima redirecta na `detail` za komentar/sliku. 
- Omogućiti da klik na `Approve` tipku u sučelju za moderiranje ne redirecta na
  `detail` view, nego da vrati korisnika nazad na sučelje.


